import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadMyFile {
    Scanner scanner = null;
    ArrayList<File> fileArrayList = new ArrayList<>();

    public ReadMyFile() throws FileNotFoundException {
     //   txtReader();
    }

    public String fileReaderAlgoritm(String file) throws IOException {
        File file1 = new File(file);
        FileReader fileReader = new FileReader(file1);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }
        fileReader.close();
        bufferedReader.close();

        return file;
    }
    //Testing the method
   /* public void throwInFilesToRead() throws IOException {
        fileReaderAlgoritm(Constants.LOREM_TXT);
    }*/

    /*private void txtReader() throws FileNotFoundException {

        //fileArrayList.add(Constants.LOREM_TXT);
        fileArrayList.add(Constants.NAME_TXT);

        for (File temp : fileArrayList) {
            scanner = new Scanner(temp);
            while (scanner.hasNextLine()) {
                String rowsFromTxt = (scanner.nextLine());
                System.out.println(rowsFromTxt);
            }
        }
        scanner.close();
    }*/

}
