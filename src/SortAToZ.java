import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class SortAToZ {
    Scanner scanner = null;
    ReadMyFile readMyFile;
    public SortAToZ() throws IOException {
        try {
            sortingMethod();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void sortingMethod() throws IOException {
        //String[] wordByWordList = txtReader(Constants.NAME_TXT).toString().trim().split("\\s+");
        String s = readMyFile.fileReaderAlgoritm(Constants.LOREM_TXT);
        List<String> sortedArr = new ArrayList<>();

        while (scanner.hasNextLine()) {

            sortedArr.add(scanner.nextLine().trim().strip());
        }
        scanner.close();

        Collections.sort(sortedArr);
//        System.out.println(sortedArr.toString());
        System.out.println("After sort");
        for (String counter : sortedArr) {

            System.out.println(counter);
        }

    }
}
