import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class Main {
    static Scanner sc = new Scanner(System.in);
    static ArrayList<File> fileArrayList = new ArrayList<>();
    public static Scanner scanner = null;
    static ReadMyFile readMyFile;

    static {
        try {
            readMyFile = new ReadMyFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        chooseMenu();
    }

    public static void chooseMenu() {
        while (true) {
            System.out.println("\n[1]  \n[2]  Read file \n[3]  " +
                    "\n[4]      \n[5]  ");

            try {
                sc = new Scanner(System.in);
                int choice = sc.nextInt();
                switch (choice) {
                    case 1:
                        break;
                    case 2:
                        readMyFile.fileReaderAlgoritm(Constants.LOREM_TXT);
                        break;
                    case 3:
                        new SortAToZ();
                        break;
                    case 4:
                        break;
                    case 5:
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Choose from available options 1,2,3,4");
                }

            } catch (InputMismatchException | FileNotFoundException e) {
                System.out.println("Choose from menu and try with integer.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //addToArray();
        //txtReader();
        //searcher();
        //sortingMethod();
    }

}








